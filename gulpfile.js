var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();


gulp.task('sass', function() {
  return gulp.src('scss/main.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('public/css'))
})

gulp.watch('scss/**/*.scss', ['sass']); 

gulp.task('browserSync', function() {
  browserSync.init(["css/*.css", "js/*.js"], {
    server: {
      baseDir: './public'
    },
  })
})

gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('scss/**/*.scss', ['sass']); 
  // Reloads the browser whenever HTML or JS files change
  gulp.watch('scss/**/*.scss', browserSync.reload); 
  gulp.watch('public/*.html', browserSync.reload); 
  gulp.watch('public/js/**/*.js', browserSync.reload); 
});